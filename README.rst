=====================
What is Yombo Gateway
=====================

The Yombo Gateway is an open-source framework to provide automation of various
devices (X10, Z-Wave, Insteon, etc) from any manufacturer.  Yombo provides a
way out of vendor lock-in by allowing cross-vendor and cross-protocol bridging.

=============
Documentation
=============

Installation instructions, getting started and in-depth API documentation can
be found here:

http://www.yombo.net/docs/gateway/current/

===========
Quick Start
===========

1. You must have a `Yombo account <http://www.yombo.net>`_ .
2. Prepare your os. Need to install python and various python modules.
   See http://www.yombo.net/docs/gateway/current/chapters/prepare-operating-system.html
3. Configure the gateway: ./config.py
4. You can run the Gateway from the local directory (thumb drive, etc)
   or install and have it start at bootup. The install folder contains
   install-*-service script to setup the Gateway software and requests the
   system start it up bootup.  Run with: `sudo ./install-OSVERSION-service.sh`

===============================
Getting Help / Other Resources
===============================

For issue (tickets), feature requests, forums,  and wiki articles, visit
`Yombo Gateway Projects <https://projects.yombo.net/projects/gateway>`_ page.

=========================
Yombo Gateway License 
=========================

By accessing Yombo code, you are agreeing to the following licensing terms. 
If you do not agree to these terms, do not access the code.

Your license to the Yombo Gateway source and/or binaries is governed by the
Reciprocal Public License 1.5 license as described here in the included
LICENSE file distributed with this software.

If you do not wish to release the source of software you build using Yombo
Gateway, you may use Yombo Gateway source and/or binaries under the Yombo
Gateway End User License Agreement as described here:

http://www.yombo.net/policies/licensing/yombo-private-license
