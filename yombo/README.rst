=====================
Yombo Directory
=====================

Contains the core functions, libraries to run the gateway
and a place to put modules.  See online documentation for
more details.
http://www.yombo.net/docs/gateway/current/chapters/yombo-framework.html

core
------

Contains base functions and various Yombo Gateway APIs.

lib
----------

Responsible for running the Yombo Gateway, and contains some APIs.

modules
----------
Where modules are installed by the Gateway.
