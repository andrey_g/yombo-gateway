Empty Module
==================

This module is primarily used to create new modules from. Why start from
scratch when you can start with a basic structure?

New to developing modules for Yombo gateway? No fear, check out the tutorial
at https://projects.yombo.net/projects/modules/wiki/Building_you_first_module
