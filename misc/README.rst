=====================
Misc Directory
=====================

The misc directory contains misc tools and files.

memusg
------

memusg - Used to monitor memory usage.  Usage:  From the root yombo directory
execute: misc/memusg ./yombo.sh

When Yombo Gateway exist, the peak memory usage will be displayed.
