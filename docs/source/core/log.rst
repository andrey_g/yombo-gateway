.. index:: log

.. _log:

.. currentmodule:: yombo.core.log

================================
Logging (yombo.core.log)
================================

Log Module
==============================
.. automodule:: yombo.core.log
   :members:

   .. automethod:: __init__



