.. Documentation master file, created on Tue Jul 10 13:38:03 2012.

Yombo Gateway Documentation
===========================
Welcome to the open source Yombo Gateway documentation. Get your gateway
up and running in no time.  Just three steps:

* Install python and required modules
* Download the gateway (Doesn't require special installation, can run on thumb
  drive.)
* Configure your gateway.

Other Resouces
==============
* `Yombo.net <http://www.yombo.net/>`_ - Main website for Yombo
* `Projects.Yombo.net <http://projects.yombo.net/>`_ - Forums, submit tickets,
  feature requests, wiki.
* `Code.Yombo.net <http://code.yombo.net/>`_ - Where all the Yombo open source
  code lives.

For more information about Yombo or the API interface for accessing
various features of the Yombo Gateway remotely, visit the Yombo web
site at `<http://www.yombo.net>`_.

Table of Contents
=================

.. toctree::
   :maxdepth: 2
   :numbered:

   chapters/prepare-operating-system.rst
   chapters/install-gateway.rst
   chapters/running-the-gateway.rst
   chapters/yombo-framework.rst
   chapters/developing-yombo-modules.rst

Last updated: |today|

Contributing to Documentation
=============================

Yombo uses self documenting code. This document is generated directly from the
source code itself, as well as a few index files (such as this page).

Making changes is easy: simply find the section of the code and update the
code comment. This website is periodically updated from the source code.

If you wish to make changes to the index pages, the source code to that is
located in : docs/source/

Index and search
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

License Information
===================

:Homepage: http://www.yombo.net/
:Copyright: 2013 Yombo
:License: For the open source license, see the LICENSE file or http://www.yombo.net/policies/licensing
